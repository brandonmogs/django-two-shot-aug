from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/receipt_list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    return render(request, "receipts/create_receipt.html", {"form": form})


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    category_receipt_counts = {}

    for category in categories:
        receipt_count = Receipt.objects.filter(
            category=category, purchaser=request.user
        ).count()
        category_receipt_counts[category] = receipt_count

    return render(
        request,
        "receipts/category_list.html",
        {"categories": categories, "receipt_counts": category_receipt_counts},
    )


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    account_receipt_counts = {}

    for account in accounts:
        receipt_count = Receipt.objects.filter(
            account=account, purchaser=request.user
        ).count()
        account_receipt_counts[account] = receipt_count

    return render(
        request,
        "receipts/account_list.html",
        {"accounts": accounts, "receipt_counts": account_receipt_counts},
    )


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    return render(request, "receipts/create_category.html", {"form": form})


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    return render(request, "receipts/create_account.html", {"form": form})
