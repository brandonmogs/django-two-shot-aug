from django.urls import path
from . import views
from django.shortcuts import redirect


def redirect_to_receipt_list(request):
    return redirect("receipts")


urlpatterns = [
    path("", views.receipt_list, name="home"),
    path("create/", views.create_receipt, name="create_receipt"),
    path("categories/", views.category_list, name="category_list"),
    path("accounts/", views.account_list, name="account_list"),
    path("categories/create/", views.create_category, name="create_category"),
    path("accounts/create/", views.create_account, name="create_account"),
]
